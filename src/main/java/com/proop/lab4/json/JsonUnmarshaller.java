package com.proop.lab4.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proop.lab4.shape.Shape;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class JsonUnmarshaller {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    static {
        OBJECT_MAPPER.enableDefaultTyping();
    }
    private static final TypeReference<LinkedList<Shape>> SHAPE_LIST_TYPE_REFERENCE = new TypeReference<LinkedList<Shape>>(){};

    public List<Shape> read(File file) throws IOException {
        try {
            return OBJECT_MAPPER.readValue(file, SHAPE_LIST_TYPE_REFERENCE);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to read shapes from file: " + file.getName());
            throw e;
        }
    }

}
