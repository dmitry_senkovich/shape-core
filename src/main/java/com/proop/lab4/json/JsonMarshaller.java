package com.proop.lab4.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.proop.lab4.shape.Shape;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonMarshaller {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    static {
        OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
        OBJECT_MAPPER.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        OBJECT_MAPPER.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        OBJECT_MAPPER.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
    }

    public void save(File file, List<Shape> shapes) throws IOException {
        try {
            OBJECT_MAPPER.writeValue(file, shapes);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to save shapes to file: " + file.getName());
            throw e;
        }
    }

}
