package com.proop.lab4.painter;

import com.proop.lab4.shape.Shape;

public interface ShapePainter<S extends Shape> {

    String paint(Integer[] coordinates);

}
