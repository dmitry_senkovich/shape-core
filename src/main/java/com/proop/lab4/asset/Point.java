package com.proop.lab4.asset;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Point implements Serializable {

    private Integer x;
    private Integer y;

    @JsonCreator
    public Point(@JsonProperty("x") Integer x, @JsonProperty("y") Integer y) {
        this.x = x;
        this.y = y;
    }


    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("(%d, %d)", x, y);
    }

}
