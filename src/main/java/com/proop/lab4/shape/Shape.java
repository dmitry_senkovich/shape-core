package com.proop.lab4.shape;

import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;

import java.io.Serializable;
import java.util.List;

public interface Shape extends Serializable {

    Integer[] getCoordinates();

    String getShapeName();

    List<Point> getPoints();
    void setPoints(List<Point> points);
    List<Line> getLines();
    void setLines(List<Line> lines);

}
